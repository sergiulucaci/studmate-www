Step 1. Install dependencies:
```
npm install
```

Step 2. Run the app (default port is 3000):
```
npm run start:dev
```