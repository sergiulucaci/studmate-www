import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import Category from "./Category/reducer";

export default combineReducers({
  Category,
  routing: routerReducer
});
