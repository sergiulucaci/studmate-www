import CategoryApi from "./api";
import {__getAll, __get, __add, __edit, __update} from "./actionsCreators";
import {browserHistory} from "react-router";

/**
 * Get All Categories
 * @param meta
 * @returns {function(*, *)}
 */
const getAll = (meta) => {
  return (dispatch, getState) => {
    dispatch(__getAll.pending());

    CategoryApi.getAll(meta).then(function(data) {
      dispatch(__getAll.success(data));

    }).catch(function(error) {
      dispatch(__getAll.error(error));

    });
  }
};

/**
 * Get Category
 * @param id
 * @returns {function(*, *)}
 */
const get = (id) => {
  return (dispatch, getState) => {
    dispatch(__get.pending());

    CategoryApi.get(id).then(function(data) {
      dispatch(__get.success(data));

    }).catch(function(error) {
      dispatch(__get.error(error));

    });
  }
};

/**
 * Add Category
 * @param category
 * @returns {function(*, *)}
 */
const add = (category) => {
  return (dispatch, getState) => {
    dispatch(__add.pending());

    CategoryApi.add(category).then(function(data) {
      dispatch(__add.success(data));
      browserHistory.push({
        pathname: '/categories'
      });
      dispatch(__edit.clear());

    }).catch(function(error) {
      dispatch(__add.error(error));

    });
  }
};

/**
 * Edit Category
 * @param category
 * @returns {function(*, *)}
 */
const edit = (category) => {
  return (dispatch, getState) => {
    dispatch(__edit.default(category));
  }
};

/**
 * Edit Clear Category
 * @param category
 * @returns {function(*, *)}
 */
const editClear = (category) => {
  return (dispatch, getState) => {
    dispatch(__edit.clear());
  }
};

/**
 * Update Category
 * @param category
 * @returns {function(*, *)}
 */
const update = (category) => {
  return (dispatch, getState) => {
    dispatch(__update.pending());

    CategoryApi.update(category).then(function(data) {
      dispatch(__update.success(data));
      browserHistory.push({
        pathname: '/categories'
      });
      dispatch(__edit.clear());

    }).catch(function(error) {
      dispatch(__update.error(error));

    });
  }
};

export default {
  getAll,
  get,
  add,
  edit,
  editClear,
  update
}