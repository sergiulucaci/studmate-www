import CategoryConstants from "./constants";
import _set from "lodash/set";

const initialState = {
  all: {
    data: [],
    meta: {}
  },
  current: {
    name: ''
  }
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    /**
     * GET ALL
     */
    case CategoryConstants.GET_ALL_SUCCESS:
      return {
        ...state,
        all: Object.assign({}, state.all, action.payload)
      };

    /**
     * GET
     */
    case CategoryConstants.GET_SUCCESS:
      return {
        ...state,
        current: Object.assign({}, state.current, action.payload)
      };

    /**
     * EDIT
     */
    case CategoryConstants.EDIT:
      let currentCopy = Object.assign({}, state.current);
      _set(currentCopy, action.payload.path, action.payload.value);
      return {
        ...state,
        current: currentCopy
      };

    /**
     * EDIT CLEAR
     */
    case CategoryConstants.EDIT_CLEAR:
      return {
        ...state,
        current: initialState.current
      };

    /**
     * UPDATE
     */
    case CategoryConstants.UPDATE_SUCCESS:
      return {
        ...state,
        current: Object.assign({}, state.current, action.payload)
      };

    default:
      return state;
  }
};