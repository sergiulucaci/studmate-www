import request from "superagent";
import {API_HOST} from "../../config";

/**
 * Get All Categories
 * @param meta
 * @returns {Promise}
 */
const getAll = (meta) => {
  return new Promise(function(resolve, reject) {
    request
      .get(API_HOST + '/categories')
      .query(meta)
      .end(function(err, res) {
        if (err) return reject(err);
        if (res) {
          if (!res.ok) return reject(res.body);
          resolve(res.body);
        }
      })
  })
};

/**
 * Get Category
 * @param id
 * @returns {Promise}
 */
const get = (id) => {
  return new Promise(function(resolve, reject) {
    request
      .get(API_HOST + '/categories/' + id)
      .end(function(err, res) {
        if (err) return reject(err);
        if (res) {
          if (!res.ok) return reject(res.body);
          resolve(res.body.data);
        }
      })
  })
};

/**
 * Add Category
 * @param category
 * @returns {Promise}
 */
const add = (category) => {
  return new Promise((resolve, reject) => {
    request
      .post(API_HOST + '/categories/add/')
      .send(category)
      .end(function(err, res) {
        if (err) return reject(err);
        if (res) {
          if (!res.ok) return reject(res.body);
          resolve(res.body);
        }
      })
  });
};

/**
 * Update Category
 * @param category
 * @returns {Promise}
 */
const update = (category) => {
  return new Promise((resolve, reject) => {
    request
      .put(API_HOST + '/categories/' + category.id)
      .send(category)
      .end(function(err, res) {
        if (err) return reject(err);
        if (res) {
          if (!res.ok) return reject(res.body);
          resolve(res.body.data);
        }
      })
  });
};

/**
 * Delete Category
 * @param id
 * @returns {Promise}
 */
const remove = (id) => {
  return new Promise(function(resolve, reject) {
    request
      .delete(API_HOST + '/categories/' + id)
      .end(function(err, res) {
        if (err) return reject(err);
        if (res) {
          if (!res.ok) return reject(res.body);
          resolve(res.body.data);
        }
      })
  })
};


export default {
  getAll,
  get,
  add,
  update,
  remove
}