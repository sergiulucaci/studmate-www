import CategoryConstants from "./constants";

/**
 * GET ALL
 */
export const __getAll = {
  pending: () => ({
    type: CategoryConstants.GET_ALL_PENDING
  }),
  success: (data) => ({
    type: CategoryConstants.GET_ALL_SUCCESS,
    payload: data
  }),
  error: (err) => ({
    type: CategoryConstants.GET_ALL_ERROR,
    payload: err
  })
};

/**
 * GET
 */
export const __get = {
  pending: () => ({
    type: CategoryConstants.GET_PENDING
  }),
  success: (data) => ({
    type: CategoryConstants.GET_SUCCESS,
    payload: data
  }),
  error: (err) => ({
    type: CategoryConstants.GET_ERROR,
    payload: err
  })
};

/**
 * ADD
 */
export const __add = {
  pending: (category) => ({
    type: CategoryConstants.ADD_PENDING
  }),
  success: (data) => ({
    type: CategoryConstants.ADD_SUCCESS,
    payload: data
  }),
  error: (err) => ({
    type: CategoryConstants.ADD_ERROR,
    payload: err
  })
};

/**
 * EDIT
 */
export const __edit = {
  default: (data) => ({
    type: CategoryConstants.EDIT,
    payload: data
  }),
  clear: () => ({
    type: CategoryConstants.EDIT_CLEAR
  })
};

/**
 * UPDATE
 */
export const __update = {
  pending: (category) => ({
    type: CategoryConstants.UPDATE_PENDING
  }),
  success: (data) => ({
    type: CategoryConstants.UPDATE_SUCCESS,
    payload: data
  }),
  error: (err) => ({
    type: CategoryConstants.UPDATE_ERROR,
    payload: err
  })
};

export default {
  getAll: __getAll,
  get: __get,
  add: __add,
  edit: __edit,
  update: __update
}