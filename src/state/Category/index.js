import api from "./api";
import actions from "./actions";
import constants from "./constants";
import reducer from "./reducer";
import selectors from "./selectors";

export default {
  api,
  actions,
  constants,
  reducer,
  selectors
};