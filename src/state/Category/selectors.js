import {createSelector} from "reselect";

/* Input Selectors */

const __all = (state) => state.Category.all;
const __current = (state) => state.Category.current;

/* Memoized Selectors */

/**
 * All
 */
const all = createSelector([__all], (all) => {
  return all;
});

/**
 * Current
 */
const current = createSelector([__current], (current) => {
  return current;
});

export default {
  all,
  current
};