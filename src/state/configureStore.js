import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import createLogger from "redux-logger";
import rootReducer from "./rootReducer";
import {IS_DEVELOPMENT} from "../config";

export default function configureStore(initialState) {
  const logger = createLogger({
    collapsed: true,
    logger: console
  });

  let middlewares = applyMiddleware(thunk);
  if (IS_DEVELOPMENT) middlewares = applyMiddleware(thunk, logger);

  return createStore(rootReducer, initialState, middlewares);
};
