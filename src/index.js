import React from "react";
import ReactDOM from "react-dom";
import Root from "./modules/Root";
import {browserHistory} from "react-router";
import configureStore from "./state/configureStore";

ReactDOM.render(
  <Root history={browserHistory} store={configureStore()}/>,
  document.getElementById('root')
);
