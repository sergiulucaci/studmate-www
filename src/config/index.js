export const IS_DEVELOPMENT = process.env.NODE_ENV === 'development';
export const API_HOST = IS_DEVELOPMENT ? 'http://localhost:8000' : '/api';
