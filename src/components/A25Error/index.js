import React, {Component} from "react";
import "./style.css";

/**
 * Class A25Error
 */
class A25Error extends Component {

  /**
   * Render
   *
   * @returns {XML}
   */
  render () {
    let props = this.props;
    let message = props.message ? props.message : 'Required.';

    return (
      <div className="A25ErrorComponent">
        <p>{message}</p>
      </div>
    )
  }
}

export default A25Error
