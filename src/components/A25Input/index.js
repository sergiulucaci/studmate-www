import React, {Component} from "react";
import "./style.css";
import A25Error from "../A25Error";

/**
 * Class A25Input
 */
class A25Input extends Component {

  constructor(props) {
    super(props);
    this.changeHandler = this.changeHandler.bind(this);
  }

  renderLabel () {
    let props = this.props;
    return (
      <div>{props.label}</div>
    )
  }

  changeHandler(e) {
    e.preventDefault();
    this.props.onChange(e.target.value);
  }

  renderInput() {
    let props = this.props;

    if (!props.readOnly) {
      return (
        <input className="input"
               id={props.id}
               type={props.type ? props.type : 'text'}
               value={props.value}
               onChange={props.onChange}
               defaultValue={props.defaultValue}
               placeholder={props.placeholder}
               max={props.type === 'number' && props.max ? props.max : ''}
               min={props.type === 'number' && props.min ? props.min : ''}/>
      )
    } else {
      return (
        <div className="readOnly">{props.value}</div>
      )
    }
  }

  renderError () {
    let props = this.props;
    if (props.required && !props.value && props.status) {
      return (
        <A25Error message="Required." />
      )
    }
  }

  renderLayout(label, input, error) {
    let props = this.props;

    if (props.label) {
      return (
        <div className="row">
          <div className="col-xs-3">
            {label}
          </div>
          <div className="col-xs-9">
            {input}
            {error}
          </div>
        </div>
      )
    }

    return (
      <div>
        {input}
        {error}
      </div>
    )
  }

  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    let props = this.props;

    return (
      <div className={`A25InputComponent ${props.className ? props.className : ''}`}>
        {this.renderLayout(this.renderLabel(), this.renderInput(), this.renderError())}
      </div>
    )
  }
}

export default A25Input