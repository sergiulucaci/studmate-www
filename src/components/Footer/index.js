import React, {Component} from "react";
import "./style.css";

/**
 * Class Footer
 *
 * @package Footer
 * @version 0.1.0
 */
class Footer extends Component {
  /**
   * Render
   *
   * @returns {XML}
   */
  render() {

    return (
      <div className="FooterComponent">
        <h1>Footer</h1>
      </div>
    )
  }
}

export default Footer
