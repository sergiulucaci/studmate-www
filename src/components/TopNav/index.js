import React, {Component} from "react";
import "./style.css";

/**
 * Class TopNav
 *
 * @package TopNav
 * @version 0.1.0
 */
class TopNav extends Component {

  /**
   * Render
   *
   * @returns {XML}
   */
  render () {
    return (
      <div className="TopNavComponent">
        <div className="container">
          <nav className="navbar navbar-toggleable-sm navbar-light">
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"/>
            </button>
            <a className="navbar-brand" href="/">logo</a>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link" href="/categories">Category</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    )
  }
}

export default TopNav
