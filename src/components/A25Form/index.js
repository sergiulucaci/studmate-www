import React, {Component} from "react";
import "./style.css";

/**
 * Class A25Form
 */
class A25Form extends Component {

  componentWillMount() {
    if (this.props.getFormData) {
      this.props.getFormData();
    }
  }

  onSubmit(e) {
    let props = this.props;
    e.preventDefault();
    if (!props.readOnly) {
      props.onSubmit();
    }
  }

  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    let props = this.props;

    return (
      <form className="A25FormComponent" onSubmit={e => this.onSubmit(e)}>
        {props.children}
      </form>
    )
  }
}

export default A25Form