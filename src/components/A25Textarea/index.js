import React, {Component} from 'react'
import './style.css';
import A25Error from "../A25Error";

/**
 * Class A25Textarea
 */
class A25Textarea extends Component {

  renderLabel() {
    let props = this.props;
    return (
      <span>{props.label}</span>
    )
  }

  renderInput() {
    let props = this.props;

    if (!props.readOnly) {
      return (
        <textarea className="form-control input"
                  type={props.type ? props.type : 'text'}
                  value={props.value}
                  defaultValue={props.defaultValue}
                  placeholder={props.placeholder} name="name"
                  required="required"
                  onChange={props.onChange}/>
      )
    } else {
      return (
        <div className="readOnly">{props.value}</div>
      )
    }
  }

  renderError() {
    let props = this.props;
    if (props.required && !props.value && props.status) {
      return (
        <A25Error message="Required."/>
      )
    }
  }

  renderLayout(input, error) {
    let props = this.props;

    if (props.label) {
      return (
        <div className="row">
          <div className="col-xs-3">
            {props.label}
          </div>
          <div className="col-xs-9">
            <div className="form-group">
              {input}
              {error}
            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        {input}
        {error}
      </div>
    )
  }

  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    let props = this.props;

    return (
      <div className={`${"A25TextareaComponent"} ${props.className ? props.className : ''}`}>
        {this.renderLayout(this.renderInput(), this.renderError())}
      </div>
    )
  }
}

export default A25Textarea
