import React, {Component} from "react";
import "./style.css";

/**
 * Class A25Btn
 */
class A25Btn extends Component {

  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    let props = this.props;
    let options = {};

    if (props.disabled) {
      options.disabled = 'disabled'
    }

    return (
      <button
        className={`${"A25BtnComponent"} ${props.color ? props.color : ''} ${props.className ? props.className : ''}`}
        type={props.type ? props.type : 'button'}
        {...options}
        onClick={props.onClick}>
        {props.children}
      </button>
    )
  }
}

export default A25Btn
