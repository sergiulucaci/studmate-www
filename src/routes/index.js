import React from "react";
import {Router, Route} from "react-router";
import AppContainer from "../modules/App/containers/AppContainer";
import HomeContainer from "../modules/Home/containers/HomeContainer";
import ListContainer from "../modules/Category/containers/ListContainer";
import FormContainer from "../modules/Category/containers/FormContainer";

class Routes extends React.Component {
  render() {
    let props = this.props;

    return (
      <Router history={props.history}>
        <Route component={AppContainer}>

          {/* HOME */}
          <Route path="/" component={HomeContainer}/>

          {/* CATEGORY */}
          <Route path="/categories" component={ListContainer}/>
          <Route path="/categories/add" component={FormContainer}/>
          <Route path="/categories/:categoryId" component={FormContainer}/>

        </Route>
      </Router>
    )
  }
}

export default Routes
