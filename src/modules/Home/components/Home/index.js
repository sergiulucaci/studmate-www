import React from "react";
import "./style.css";

class Home extends React.Component {
  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    return (
      <div className="HomeComponent">
        <h1>Home</h1>
      </div>
    );
  }
}

export default Home;
