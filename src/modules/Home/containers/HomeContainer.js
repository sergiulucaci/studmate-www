import React from "react";
import HomeComponent from "../components/Home";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class HomeContainer extends React.Component {
  render() {
    let props = this.props;
    return <HomeComponent {...props}/>;
  }
}

function mapStateToProps(state, props) {
  return {
  }
}

function mapDispatchToProps(dispatch, props) {
  return bindActionCreators({
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
