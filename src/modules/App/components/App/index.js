import React from "react";
import TopNav from "../../../../components/TopNav";
import Footer from "../../../../components/Footer";
import "normalize.css/normalize.css";
import "./global.css";
import "./style.css";

class App extends React.Component {
  /**
   * Render
   *
   * @returns {XML}
   */
  render() {
    let props = this.props;

    return (
      <div className="AppComponent">
        <header>
          <TopNav {...props}/>
        </header>

        <main className="container">
          <div className="leftHalf"></div>
          <div className="rightHalf"></div>
          {props.children}
        </main>

        <footer>
          <Footer>
          </Footer>
        </footer>
      </div>
    );
  }
}

export default App;
