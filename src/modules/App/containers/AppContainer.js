import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import AppComponent from "../components/App";

class AppContainer extends React.Component {
  render() {
    let props = this.props;
    return <AppComponent {...props}/>;
  }
}

function mapStateToProps(state, props) {
  return {
  }
}

function mapDispatchToProps(dispatch, props) {
  return bindActionCreators({
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
