import React from "react";
import {Provider} from "react-redux";
import Routes from "../../routes";

class App extends React.Component {
  render() {
    let props = this.props;

    return (
      <Provider store={props.store}>
        <Routes history={props.history}/>
      </Provider>
    );
  }
}

export default App;
