import React from "react";
import "./style.css";
import A25Form from "../../../../components/A25Form";
import A25Input from "../../../../components/A25Input";

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.getFormData = this.getFormData.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  getFormData() {
    let props = this.props;
    if (props.params.categoryId) {
      props.getCategory(props.params.categoryId);
    }
  }

  onChange(path, value) {
    this.props.editCategory({path, value});
  }

  onSubmit() {
    let props = this.props;
    if (props.params.categoryId) {
      props.updateCategory(props.category);
    } else {
      props.addCategory(props.category);
    }
  }

  render() {
    let props = this.props;

    return (
      <div className="CategoryFormComponent">

        <A25Form getFormData={this.getFormData} onSubmit={this.onSubmit}>

          {/* NAME */}
          <A25Input value={props.category.name}
                    onChange={value => this.onChange('name', value)}
                    placeholder={'Nume Categorie'}
                    label={'Nume: '}
                    required={true}/>
          
        </A25Form>

      </div>
    );
  }
}

export default Form;
