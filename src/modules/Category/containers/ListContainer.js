import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import List from "../components/List";
import Category from "../../../state/Category";

class ListContainer extends React.Component {
  render() {
    return <List {...this.props}/>;
  }
}

function mapStateToProps(state, props) {
  return {
    categories: Category.selectors.all(state)
  }
}

function mapDispatchToProps(dispatch, props) {
  return bindActionCreators({
    getAllCategories: Category.actions.getAll
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
