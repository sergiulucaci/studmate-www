import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Form from "../components/Form";
import Category from "../../../state/Category";

class FormContainer extends React.Component {
  render() {
    return <Form {...this.props}/>
  }
}

function mapStateToProps(state, props) {
  return {
    category: Category.selectors.current(state)
  }
}

function mapDispatchToProps(dispatch, props) {
  return bindActionCreators({
    addCategory: Category.actions.add,
    getCategory: Category.actions.get,
    editCategory: Category.actions.edit,
    updateCategory: Category.actions.update
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);
